Tests to see how many json files can different libraries handle.

Tests load same set of json files repeatedly, keeping result objects in memory until process crashes. This test was done for Iconify API, so it uses Iconify json files.

Important: this test loads entire json file into memory. Some tested libraries are intended to be used for filtering huge json files, so this usage might not be their intended usage.

# init:

```npm install @iconify/json```

That code will load Iconify json files. Those files are not huge, but large enough to cause concern. Biggest is 24.7mb

Results might be different for bigger files.

# tests:

## json.js

JSON.parse()

## eval.js

eval()

Important: this can be used only with trusted json data

## func.js

Same as eval(), but instead of eval() it uses Function()

## json-eval.js

json-eval

```npm install json-eval```

## bfj.js

bfj

```npm install bfj```

## jsonstream.js

JSONStream and event-stream

```npm install JSONStream event-stream```

## stream-json.js

stream-json and stream-chain.

```npm install stream-json stream-chain```

# results:

Report format:
* First line is initial memory usage before loading any json files
* Each line uses this format:
	* json file counter
	* counter within current loop
	* json file that is about to be loaded
	* memory usage
* After each loop it reports "loop X" line:
	* loop counter
	* memory usage
	* time since start of process
	* number of icons found in json files (to make sure data is correct. number should always be the same)

## Vultr small VPS test

Tests were executed on CentOS 7 VPS with 512mb memory limit.

Node.js 11.0.0

### json.js

```
node json.js
60.98%
loop 1 64.23% 1.672 sec 33193 icons
loop 2 52.01% 3.192 sec 33193 icons
149 39/55: noto-v1 54.86% Killed
```

### eval.js

```
node eval
60.94%
loop 1 84.49% 1.635 sec 33193 icons
loop 2 82.14% 3.211 sec 33193 icons
loop 3 87.19% 3.913 sec 33193 icons
205 40/55: twemoji 92.39% Killed
```

### func.js

```
node func
60.94%
loop 1 84.5% 1.781 sec 33193 icons
loop 2 81.93% 3.023 sec 33193 icons
loop 3 87.04% 3.674 sec 33193 icons
205 40/55: twemoji 92.38% Killed
```

### bfj.js

```
node bfj.js
52.84%
loop 1 59.6% 70.418 sec 33193 icons
98 43/55: emojione-v1 52.83% Killed
```

### jsonstream.js

```
node jsonstream.js
44.7%
loop 1 74.5% 2.393 sec 33193 icons
loop 2 86.71% 4.225 sec 33193 icons
loop 3 87.59% 5.818 sec 33193 icons
208 43/55: emojione-v1 90.16% Killed
```

### stream-json.js

```
node stream-json.js
43.7%
loop 1 89.76% 8.1 sec 33193 icons
93 38/55: noto 90.84% Killed
```

### json-eval.js

```
node json-eval
42.7%
38 38/55: noto 91.92% Killed
```

## Vultr bigger VPS test

Tests were executed on CentOS 7 VPS with 1024mb memory limit.

Node.js 11.0.0

### json.js

```
node json.js
60.94%
loop 1 48.52% 1.261 sec 33193 icons
loop 2 52.27% 2.331 sec 33193 icons
...
loop 45 58.75% 63.602 sec 33193 icons
loop 46 64.11% 64.971 sec 33193 icons
2568 38/55: noto 67.34% Killed
```

### eval.js

```
[ssvg@vultr json-test]$ node eval
60.94%
loop 1 84.49% 1.323 sec 33193 icons
loop 2 82.37% 2.271 sec 33193 icons
...
loop 67 95.78% 29.242 sec 33193 icons
loop 68 96.12% 29.603 sec 33193 icons
3778 38/55: noto 95.6% Killed

```

### func.js

```
[ssvg@vultr json-test]$ node func
60.94%
loop 1 84.11% 1.256 sec 33193 icons
loop 2 81.49% 2.233 sec 33193 icons
...
loop 76 91.28% 33.583 sec 33193 icons
loop 77 92.45% 33.885 sec 33193 icons
4278 43/55: emojione-v1 94.3% Killed
```

### bfj.js

```
node bfj.js
52.81%
loop 1 62.65% 57.14 sec 33193 icons
loop 2 66.94% 113.995 sec 33193 icons
loop 3 65.33% 172.196 sec 33193 icons
loop 4 56.17% 231.831 sec 33193 icons
263 43/55: emojione-v1 64.95% Killed
```

### jsonstream.js

```
node jsonstream.js
44.64%
loop 1 74.05% 1.987 sec 33193 icons
loop 2 85.91% 3.349 sec 33193 icons
loop 3 90.84% 4.669 sec 33193 icons
loop 4 91.44% 5.955 sec 33193 icons
loop 5 90.96% 7.29 sec 33193 icons
loop 6 93.62% 8.581 sec 33193 icons
loop 7 93.56% 9.885 sec 33193 icons
loop 8 93.69% 11.242 sec 33193 icons
loop 9 94.58% 12.806 sec 33193 icons
538 43/55: emojione-v1 95.07% 
```

### stream-json.js

```
node stream-json.js
43.64%
loop 1 88.98% 6.696 sec 33193 icons
loop 2 94.78% 12.197 sec 33193 icons
153 43/55: emojione-v1 94.72% Killed
```

### json-eval.js

```
node json-eval
42.7%
38 38/55: noto 91.92% Killed
```

# Conclusion

Function() and eval() are best solutions with Function() providing slightly better results, however they can only be used to parse trusted json files because it is easy to exploit.

On small VPS with 512mb memory limit JSONStream seems to be best solution, matching eval() and Function() by memory usage but much much safer. JSON.parse also viable. Other solutions are not worth considering.

On bigger VPS with 1024mb memory limit Function() is best by far, JSON.parse is second best by wide margin, all stream solutions seem to be hitting memory limit quite early. From stream solutions JSONStream performs much better than anything else. Other solutions are not worth considering.

So use Function() if sources are trusted, use JSON.parse() if not. Stream solutions aren't worth it for loading entire json files.
