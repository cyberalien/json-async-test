"use strict";

const fs = require('fs');
const json = require('@iconify/json');
const JSONStream = require('JSONStream');
const es = require('event-stream');

const prefixes = Object.keys(json.collections());

const mem = () => {
    let mem = process.memoryUsage();
    return Math.round(mem.heapUsed / mem.heapTotal * 10000) / 100 + '%';
};

let items = {},
    index = -1,
    key = -1,
    icons = 0,
    total = 0,
    start = Date.now();

const parse = (key, prefix) => {
    let stream = fs.createReadStream(json.locate(prefix), 'utf8');
    stream.on('error', err => {
        console.log('Error', err);
    });

    stream.pipe(JSONStream.parse(true)).pipe(es.mapSync(data => {
        items[key][prefix] = data;
        icons += Object.keys(data.icons).length;
        process.nextTick(next);
    }));
};

const next = () => {
    index ++;
    if (index === prefixes.length) {
        process.stdout.write("\r\x1b[K");
        nextKey();
        return;
    }
    process.stdout.write((index > 0 ? "\r\x1b[K" : '') + total + ' ' + index + '/' + prefixes.length + ': ' + prefixes[index] + ' ' + mem() + ' ');
    total ++;
    process.nextTick(() => {
        parse('data' + key, prefixes[index]);
    });
};

const nextKey = () => {
    key ++;
    items['data' + key] = {};
    if (key > 0) {
        console.log('loop', key, mem(), (Date.now() - start) / 1000, 'sec', icons, 'icons');
    }
    index = -1;
    icons = 0;
    next();
};

console.log(mem());

nextKey();
