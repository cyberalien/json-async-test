"use strict";

const fs = require('fs');
const json = require('@iconify/json');

const prefixes = Object.keys(json.collections());

const mem = () => {
    let mem = process.memoryUsage();
    return Math.round(mem.heapUsed / mem.heapTotal * 10000) / 100 + '%';
};

let items = {},
    index = -1,
    key = -1,
    icons = 0,
    total = 0,
    start = Date.now();

const parse = (key, prefix) => {
    fs.readFile(json.locate(prefix), 'utf8', (err, data) => {
        if (err) {
            console.log(err);
            return;
        }
        eval('data = ' + data);
        icons += Object.keys(data.icons).length;
        items[key][prefix] = data;
        next();
    });
};

const next = () => {
    index ++;
    if (index === prefixes.length) {
        process.stdout.write("\r\x1b[K");
        nextKey();
        return;
    }
    process.stdout.write((index > 0 ? "\r\x1b[K" : '') + total + ' ' + index + '/' + prefixes.length + ': ' + prefixes[index] + ' ' + mem() + ' ');
    total ++;
    process.nextTick(() => {
        parse('data' + key, prefixes[index]);
    });
};

const nextKey = () => {
    key ++;
    items['data' + key] = {};
    if (key > 0) {
        console.log('loop', key, mem(), (Date.now() - start) / 1000, 'sec', icons, 'icons');
    }
    index = -1;
    icons = 0;
    next();
};

console.log(mem());

nextKey();