"use strict";

const fs = require('fs');
const {parser} = require('stream-json');
const {chain} = require('stream-chain');
const json = require('@iconify/json');
const {streamValues} = require('stream-json/streamers/StreamValues');

const prefixes = Object.keys(json.collections());

const mem = () => {
    let mem = process.memoryUsage();
    return Math.round(mem.heapUsed / mem.heapTotal * 10000) / 100 + '%';
};

let items = {},
    index = -1,
    key = -1,
    icons = 0,
    total = 0,
    start = Date.now();

const parse = (key, prefix) => {
    let pipe = chain([
        fs.createReadStream(json.locate(prefix), 'utf8'),
        parser(),
        streamValues(),
        data => {
            items[key][prefix] = data.value;
            icons += Object.keys(data.value.icons).length;
            next();
        }
    ]);

    pipe.on('error', err => {
        console.log('got error parsing collection:', prefix);
    });
};

const next = () => {
    index ++;
    if (index === prefixes.length) {
        process.stdout.write("\r\x1b[K");
        nextKey();
        return;
    }
    process.stdout.write((index > 0 ? "\r\x1b[K" : '') + total + ' ' + index + '/' + prefixes.length + ': ' + prefixes[index] + ' ' + mem() + ' ');
    total ++;
    process.nextTick(() => {
        parse('data' + key, prefixes[index]);
    });
};

const nextKey = () => {
    key ++;
    items['data' + key] = {};
    if (key > 0) {
        console.log('loop', key, mem(), (Date.now() - start) / 1000, 'sec', icons, 'icons');
    }
    index = -1;
    icons = 0;
    next();
};

console.log(mem());

nextKey();
